//! A layered mail service
use crate::common::*;
use crate::model::mail::*;
use crate::service::mail::composite::*;
use crate::service::mail::dirmail::EnableEightBit;
use crate::service::mail::*;

// TODO(sicrs): Find a more efficient way of handling hooks ?

/// ## `ExtMailService`
/// An trait for creating chainable extensions
/// Can be used to handle writes to a `Write` Sink or handle `Transaction`s by means of `Hook`s.
/// State can be saved by modifying values to the original struct through references (`Arc`)
/// `move`d inside `Hook` closures
pub trait ExtMailService {
    fn name() -> String;
    /// Returns a tuple of `Hook`s, with the first acting as a pre-`send_mail` hook
    /// and the second a a pre-, post-, or prepos- hook for write
    fn hooks<T: Write + Send + Sync + 'static>(&self) -> (Option<Hook<T>>, Option<Hook<T>>);

    /// Used to generate an `ExtMSFactory`, handles self-initialisation
    fn factory<E: ExtMailService>(self) -> ExtMSFactory<E>;
}

/// ## `Hook`
/// Action hooks to allow modification of MailService returns
// T represents the underlying dispatch object
#[derive(Debug)]
pub enum Hook<T: Write + Send + Sync + 'static> {
    /// `Hook::Dispatch` is is a pre-`send_mail` hook.
    Dispatch(Box<dyn Fn(Transaction) -> Pin<Box<dyn Future<Output = Transaction>>>>),
    PreSink(Box<dyn Fn(T) -> T>),
    PostSink(Box<dyn Fn(T) -> T>),
    PrePostSink(Box<dyn Fn(T) -> T>, Box<dyn Fn(T) -> T>),
}

// Chain extensions internally? or generate separate MailService implementations?
/// Allows the creation of modular mail services outside the `samotop` crate.
pub struct ExtMSFactory<E: ExtMailService>(pub E);

impl<E, ES, GS, DS> MailSetup<ES, GS, DS> for ExtMSFactory<E>
where
    E: ExtMailService,
    ES: EsmtpService,
    GS: MailGuard,
    DS: MailDispatch,
{
    type Output = CompositeMailService<EnableEightBit<ES>, GS, ExtensibleMailService<DS>>;
    fn setup(self, extend: ES, guard: GS, dispatch: DS) -> Self::Output {
        let extension = self.0;
        let hooks = extension.hooks();

        // Validate hooks
        use Hook::Dispatch;
        let dispatch_hook = match hooks.0 {
            hook @ Some(Dispatch(_)) | hook @ None => hook,
            _ => panic!("Cannot use non-PostDispatch hook for post dispatch"),
        };

        let sink_hook = match hooks.1 {
            hook @ Some(Dispatch(_)) => panic!("Cannot use a PostDispatch hook for sink hooks"),
            hook @ _ => hook,
        };

        // Construct the CompositeMailService
        (
            extend,
            guard,
            ExtensibleMailService::new(dispatch, dispatch_hook, sink_hook),
        )
            .into()
    }
}

// Implementation of modularity?
struct ExtensibleMailService<DS: MailDispatch> {
    // allow passing on to underlying dispatch
    underlying_dispatch: DS,
    // hooks handling underlying MailFuture and Mail associated types
    dispatch_hook: Option<Hook<DS::Mail>>,
    sink_hook: Option<Hook<DS::Mail>>,
}

impl<DS> ExtensibleMailService<DS>
where
    DS: MailDispatch,
{
    /// The constructor
    fn new(
        dispatch: DS,
        dispatch_hook: Hook<DS::Mail>,
        sink_hook: Hook<DS::Mail>,
    ) -> ExtensibleMailService<DS> {
        ExtensibleMailService {
            underlying_dispatch: dispatch,
            dispatch_hook,
            sink_hook,
        }
    }
}

impl<DS> MailDispatch for ExtensibleMailService<DS>
where
    DS: MailDispatch,
{
    // Passthrough modification
    type Mail = ExtMSFutOutput<DS::Mail>;
    type MailFuture = ExtMSMailFut<DS>;
    fn send_mail(&self, transaction: Transaction) -> Self::MailFuture {
        ExtMSMailFut {
            dispatch_hook: self.dispatch_hook,
            sink_hook: self.sink_hook,
            transaction,
            underlying: self.underlying_dispatch,
        }
        /*
        // self.underlying_dispatch.send_mail() does return an object of type `DS::MailFuture`
        // which resolves to `DispatchResult<DS::Mail>`
        let transaction: Transaction = if let Some(hook) = &self.dispatch_hook {
            // hook.0 (Closure) construct a future (stateful)
            ready!((hook.0)(transaction))
        } else {
            transaction
        };

        trace!("Pre-transaction hook: {:?}", self.dispatch_hook);

        // Send transaction
        self.underlying_dispatch.send_mail(transaction);
        */
    }
}

// TODO(sicrs): just use a T
#[pin_project(project = ExtMSMailFutProjection)]
struct ExtMSMailFut<DS: MailDispatch> {
    dispatch_hook: Option<Hook<DS::Mail>>,
    sink_hook: Option<Hook<DS::Mail>>,
    transaction: Transaction,
    underlying: DS,
}

impl<DS: MailDispatch> Future for ExtMSMailFut<DS> {
    type Output = DispatchResult<ExtMSFutOutput<DS::Mail>>;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let ExtMSMailFutProjection {
            dispatch_hook,
            sink_hook,
            transaction,
            underlying,
        } = self.project();

        // run the pre-send_mail hook, if any
        let transaction: Transaction = match dispatch_hook {
            None => transaction,
            Some(hook) => ready!((hook.0)(transaction)),
        };

        // construct ExtMSFutOutput with ready future from underlying.send_mail
        // wait for ready Future::Output which implements AsyncWrite
        let inner: DS::Mail = ready!(underlying.send_mail(transaction));

        ExtMSFutOutput::new(inner, sink_hook)
    }
}

#[pin_project(project = ExtMSFutOutProjection)]
struct ExtMSFutOutput<T: Write + Send + Sync + 'static> {
    #[pin]
    inner: T,
}

impl<T: Write + Send + Sync + 'static> ExtMSFutOutput<T> {
    fn new(inner: T, sink_hook: Option<Hook<T>>) -> ExtMSFutOutput<T> {
        let inner = match sink_hook {
            None => inner,
            Some(Hook::PostSink(hook)) | 
            Some(Hook::PreSink(hook)) => { (hook)(inner) },
            Some(Hook::PrePostSink(pre_hook, post_hook)) => {
                (pre_hook)((post_hook)(inner))
            },
            _ => unreachable!()
        };

        ExtMSFutOutput { inner }
    }
}

impl<T: Write + Send + Sync + 'static> Write for ExtMSFutOutput<T> {
    fn poll_write(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &[u8])
            -> Poll<std::io::Result<usize>> {
        let ExtMSFutOutProjection { inner } = self.project();
        inner.poll_write(cx, buf)
    }
    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        let ExtMSFutOutProjection { inner } = self.project();
        inner.poll_flush(cx)
    }
    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        let ExtMSFutOutProjection { inner } = self.project();
        inner.poll_close(cx)
    }
    
}
